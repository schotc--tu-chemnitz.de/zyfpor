/*
 * ZYFPOR - Zynq Flexible Platform for Object Recognition
 * zyfpor_ip_top.h
 *
 *
 * Author	: Christian Schott
 * E-Mail	: christian.schott@etit.tu-chemnitz.de
 * Company	: University of Technology Chemnitz
 *
 * derived from Xilinx XAPP1167
 *
 * created on: April 22, 2015
 *
 */

#ifndef _ZYFPOR_IP_TOP_H_
#define _ZYFPOR_IP_TOP_H_

#include "hls_video.h"
#include "ap_int.h"
//#include "ap_fixed.h"
#include "hls_math.h"

// maximum image size
#define MAX_WIDTH  1920
//#define MAX_WIDTH 800
#define MAX_HEIGHT 1080
//#define MAX_HEIGHT 600

// maximum number of detected lines for houghlines
#define MAX_LINES 30

// typedefs video library core structures
typedef hls::stream<ap_axiu<24,1,1,1> >               AXI_STREAM_IN;
typedef hls::stream<ap_axiu<8,1,1,1> >                AXI_STREAM_OUT;

typedef hls::Mat<MAX_HEIGHT, MAX_WIDTH, HLS_8UC3>     RGB_IMAGE;
typedef hls::Mat<MAX_HEIGHT, MAX_WIDTH, HLS_8UC1>	  GRAY_IMAGE;

typedef ap_fixed<16,3,AP_RND>						  t_lines_angle;
typedef ap_fixed<16,16,AP_RND>						  t_lines_rho;

// top level function for HW synthesis
void image_filter(AXI_STREAM_IN& src_axi, AXI_STREAM_OUT& dst_axi, int rows, int cols, unsigned int number_pixels_line, t_lines_angle lines_angle[MAX_LINES], t_lines_rho lines_rho[MAX_LINES]);

#endif
