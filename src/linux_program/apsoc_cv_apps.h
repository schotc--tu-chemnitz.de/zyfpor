/*
 * APSOC_CV
 * apsoc_cv_apps.h
 *
 * Author		:	Murali Padmanabha
 * E-Mail		:	murali.padmanabha@s2013.tu-chemnitz.de
 * Company		:	Technische Universität Chemnitz
 * 
 * Created on	:	May 1, 2015
 */

#ifndef APSOC_CV_APPS_H_
#define APSOC_CV_APPS_H_

#include <pthread.h>
#include <time.h>
#include "apsoc_cv_parameters.h"
#include "apsoc_cv_camera.h"
#include "apsoc_cv_vdma.h"
#include "apsoc_cv_filter.h"
#include "apsoc_cv_vdma.h"

void ApsocCvApp_HW_Filter(unsigned int);
void ApsocCvApp_SW_Canny(unsigned int);
void ApsocCvApp_SW_SobelXY(unsigned int);
void ApsocCvApp_SW_SobelX(unsigned int);
void ApsocCvApp_SW_FrameGrab(unsigned int);
void ApsocCvApp_HW_FilterOpt(unsigned int);

#endif /* APSOC_CV_APPS_H_ */
