/*
 * APSOC_CV
 * apsoc_cv_camera.cpp
 *
 * Author		:	Murali Padmanabha
 * E-Mail		:	murali.padmanabha@s2013.tu-chemnitz.de
 * Company		:	Technische Universität Chemnitz
 * 
 * Created on	:	Apr 24, 2015
 */

#include "apsoc_cv_camera.h"

void Camera_Open(cv::VideoCapture* vCap, int devId){
	vCap->open(devId);
	if(!vCap->isOpened()){  // check if camera device was opened
		std::cout<<"Camera device:"<<devId<<" cannot be opened"<<std::endl;
		return;
	}
}

void Camera_Open(cv::VideoCapture* vCap, const string filePath){
	vCap->open(filePath);
	if(!vCap->isOpened()){  // check if camera device was opened
		std::cout<<"Video file:"<<filePath<<" cannot be opened"<<std::endl;
		return;
	}
}
void Camera_Start_Capture(cv::VideoCapture* vCap){
	//TODO start the thread for capturing the video from the camera
}
void Camera_Get_Prop(cv::VideoCapture* vCap){
	std::cout<<"Properties of the capture device:\n";
	std::cout<<"Frame Width:\t"<<vCap->get(CV_CAP_PROP_FRAME_WIDTH)<<std::endl;
	std::cout<<"Frame Height:\t"<<vCap->get(CV_CAP_PROP_FRAME_HEIGHT)<<std::endl;
	//std::cout<<"Frame Rate:\t"<<vCap->get(CV_CAP_PROP_FPS)<<std::endl;
	std::cout<<"Codec Code:\t"<<vCap->get(CV_CAP_PROP_FOURCC)<<std::endl;
	//std::cout<<"Frame Format:\t"<<vCap->get(CV_CAP_PROP_FORMAT)<<std::endl;
	//std::cout<<"RGB Enabled:\t"<<vCap->get(CV_CAP_PROP_CONVERT_RGB)<<std::endl;
	std::cout<<"End of Property list\n";
}
void Camera_Set_Prop(cv::VideoCapture* vCap){
	//TODO set the required properties for the camera
	vCap->set(CV_CAP_PROP_FRAME_WIDTH,IN_FRAME_WIDTH);
	vCap->set(CV_CAP_PROP_FRAME_HEIGHT,IN_FRAME_HEIGHT);
	//vCap->set(CV_CAP_PROP_FPS,IN_FRAME_RATE);

}
void Camera_Stop_Capture(cv::VideoCapture* vCap){
	//TODO Kill the video capturing thread

}
void Camera_Close(cv::VideoCapture* vCap, int devId){
	vCap->release();
	if(vCap->isOpened()){  // check if camera device closed
		std::cout<<"Camera device:"<<devId<<" cannot be closed"<<std::endl;
		return;
	}
}

void Camera_Close(cv::VideoCapture* vCap, const string filePath){
	vCap->release();
	if(vCap->isOpened()){  // check if camera device closed
		std::cout<<"Video file:"<<filePath<<" cannot be closed"<<std::endl;
		return;
	}
}


