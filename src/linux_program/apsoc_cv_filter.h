/*
 * APSOC_CV
 * apsoc_cv_filter.h
 *
 * Author		:	Murali Padmanabha; Christian Schott
 * E-Mail		:	murali.padmanabha@s2013.tu-chemnitz.de; christian.schott@etit.tu-chemnitz.de
 * Company		:	Technische Universität Chemnitz
 * 
 * Created on	:	Apr 25, 2015
 */

#ifndef APSOC_CV_FILTER_H_
#define APSOC_CV_FILTER_H_

#include <fcntl.h>
#include <sys/mman.h>
#include <sys/ioctl.h>
#include <unistd.h>
#include <iostream>

#include "apsoc_cv_parameters.h"
#include "ximage_filter_hw.h"
#include "ap_int.h"
#include "ap_int_sim.h"

#define XIMAGE_FILTER_HW_BIT_SET	0x01
#define XIMAGE_FILTER_HW_BIT_CLEAR	0x00

/* Definitions of the macros */
#define REG_WRITE(addr, off, val) (*(volatile unsigned int*)(addr+off)=(val))
#define REG_READ(addr,off) (*(volatile unsigned int*)(addr+off))

/* Definition of structure for Handler */
typedef struct{
	int imageFilterHWFD;
	unsigned char*	imageFilterHWVirtAddr;
	unsigned int	imageFilterHWLength;
}ImageFilterHWHandler;

typedef ap_fixed<16,3,AP_RND> t_angle;
typedef ap_fixed<16,16,AP_RND> t_rho;
typedef struct {
	t_angle angleValue;
	t_rho rhoValue;
}HoughLineValPair;

void ImageFilter_Configure(void);
void ImageFilter_Convert(void);
void ImageFilter_Show_Status(void);
void ImageFilter_Deconfigure(void);
unsigned int ImageFilter_Read_RhoAnglePair(HoughLineValPair*);


#endif /* APSOC_CV_FILTER_H_ */
