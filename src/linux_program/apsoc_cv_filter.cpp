/*
 * APSOC_CV
 * apsoc_cv_filter.cpp
 *
 * Author		:	Murali Padmanabha; Christian Schott
 * E-Mail		:	murali.padmanabha@s2013.tu-chemnitz.de; christian.schott@etit.tu-chemnitz.de
 * Company		:	Technische Universität Chemnitz
 * 
 * Created on	:	Apr 25, 2015
 */

#include "apsoc_cv_filter.h"

ImageFilterHWHandler imageFilterHWHandler;

static void configureImageFilter(void);
static void configureImageFilterRegisters(void);

void ImageFilter_Configure(void){
	configureImageFilter();
	configureImageFilterRegisters();
}

void ImageFilter_Convert(void){
	unsigned int done;
	REG_WRITE(imageFilterHWHandler.imageFilterHWVirtAddr,XIMAGE_FILTER_CONTROL_BUS_ADDR_GIE,XIMAGE_FILTER_HW_BIT_SET);
	REG_WRITE(imageFilterHWHandler.imageFilterHWVirtAddr,XIMAGE_FILTER_CONTROL_BUS_ADDR_IER,XIMAGE_FILTER_HW_BIT_SET);
	REG_WRITE(imageFilterHWHandler.imageFilterHWVirtAddr,XIMAGE_FILTER_CONTROL_BUS_ADDR_AP_CTRL,XIMAGE_FILTER_HW_BIT_SET);
	while (1)
	{
		done = REG_READ(imageFilterHWHandler.imageFilterHWVirtAddr,XIMAGE_FILTER_CONTROL_BUS_ADDR_AP_CTRL);
		if (done >> 1 & 0x1)
			break;
	};
	REG_WRITE(imageFilterHWHandler.imageFilterHWVirtAddr,XIMAGE_FILTER_CONTROL_BUS_ADDR_GIE,XIMAGE_FILTER_HW_BIT_CLEAR);
	REG_WRITE(imageFilterHWHandler.imageFilterHWVirtAddr,XIMAGE_FILTER_CONTROL_BUS_ADDR_AP_CTRL,XIMAGE_FILTER_HW_BIT_CLEAR);
}

void ImageFilter_Deconfigure(void){
	munmap((void *)imageFilterHWHandler.imageFilterHWVirtAddr, imageFilterHWHandler.imageFilterHWLength);
	close(imageFilterHWHandler.imageFilterHWFD);
}

void ImageFilter_Show_Status(void)
{
	std::cout<<"XIMAGE_FILTER Control Register:"<<REG_READ(imageFilterHWHandler.imageFilterHWVirtAddr, XIMAGE_FILTER_CONTROL_BUS_ADDR_AP_CTRL)<<std::endl;
}

/*unsigned int ImageFilter_Read_RhoAnglePair(HoughLineValPair valPair[] ){
	unsigned int valueCount= XIMAGE_FILTER_CONTROL_BUS_DEPTH_LINES_ANGLE_V/2;
	unsigned int tempval;
	for(unsigned int i=0, j=0; i<valueCount; i++)
	{	tempval=REG_READ(imageFilterHWHandler.imageFilterHWVirtAddr, XIMAGE_FILTER_CONTROL_BUS_ADDR_LINES_RHO_V_BASE+i);
		valPair[j].rhoValue = (unsigned int)(tempval & 0X0000FFFF);
		valPair[j+1].rhoValue = (unsigned int)((tempval >> 16)& 0X0000FFFF);
		tempval=REG_READ(imageFilterHWHandler.imageFilterHWVirtAddr, XIMAGE_FILTER_CONTROL_BUS_ADDR_LINES_ANGLE_V_BASE+i);
		valPair[j].angleValue = (unsigned int)(tempval & 0X0000FFFF);
		valPair[j+1].angleValue = (unsigned int)((tempval >> 16)& 0X0000FFFF);
		j+=2 ;
	}
	return XIMAGE_FILTER_CONTROL_BUS_DEPTH_LINES_ANGLE_V;
}*/
unsigned int ImageFilter_Read_RhoAnglePair(HoughLineValPair valPair[] ){
	unsigned int valueCount= XIMAGE_FILTER_CONTROL_BUS_DEPTH_LINES_ANGLE_V/2;
	unsigned int tempval;
	unsigned int tmp1;
	unsigned int tmp2;		
	
	for(unsigned int i=0, j=0; i<valueCount; i++)
	{	tempval=REG_READ(imageFilterHWHandler.imageFilterHWVirtAddr, XIMAGE_FILTER_CONTROL_BUS_ADDR_LINES_RHO_V_BASE+i);
		tmp1 = (tempval & 0X0000FFFF);
		valPair[j].rhoValue.V = tmp1;
		tmp2 = ((tempval >> 16)& 0X0000FFFF);
		valPair[j+1].rhoValue.V = tmp2;
		tempval=REG_READ(imageFilterHWHandler.imageFilterHWVirtAddr, XIMAGE_FILTER_CONTROL_BUS_ADDR_LINES_ANGLE_V_BASE+i);
		tmp1 = (tempval & 0X0000FFFF);
		valPair[j].angleValue.V = tmp1;
		tmp2 = ((tempval >> 16)& 0X0000FFFF);
		valPair[j+1].angleValue.V = tmp2;
		j+=2 ;
	}
	return XIMAGE_FILTER_CONTROL_BUS_DEPTH_LINES_ANGLE_V;
}

static void configureImageFilter(void){
	unsigned long int PhysicalAddress =XIMAGE_FILTER_HW_BASEADDR;
	int map_len = 0x40;//XIMAGE_FILTER_HW_HIGHADDR - XIMAGE_FILTER_HW_BASEADDR;
	int fd = open( "/dev/mem", O_RDWR);
	unsigned char* base_address;

	base_address = (unsigned char*)mmap(NULL, map_len, PROT_READ | PROT_WRITE, MAP_SHARED, fd, (off_t)PhysicalAddress);
	//check if mapping was successful
	if(base_address == MAP_FAILED)
	{
		std::cout<<"Image Filter mapping memory for absolute memory access failed.\n";
		return;
	}
	imageFilterHWHandler.imageFilterHWFD = fd;
	imageFilterHWHandler.imageFilterHWVirtAddr = base_address;
	imageFilterHWHandler.imageFilterHWLength = map_len;
}

static void configureImageFilterRegisters(void){
	REG_WRITE(imageFilterHWHandler.imageFilterHWVirtAddr, XIMAGE_FILTER_CONTROL_BUS_ADDR_AP_CTRL,XIMAGE_FILTER_HW_BIT_CLEAR);
	REG_WRITE(imageFilterHWHandler.imageFilterHWVirtAddr, XIMAGE_FILTER_CONTROL_BUS_ADDR_ROWS_DATA,IN_FRAME_HEIGHT);
	REG_WRITE(imageFilterHWHandler.imageFilterHWVirtAddr, XIMAGE_FILTER_CONTROL_BUS_ADDR_COLS_DATA,IN_FRAME_WIDTH);
	REG_WRITE(imageFilterHWHandler.imageFilterHWVirtAddr, XIMAGE_FILTER_CONTROL_BUS_ADDR_NUMBER_PIXELS_LINE_DATA,FILTER_PIXEL_CNT_ON_LINE);// number of pixels on the line
}

