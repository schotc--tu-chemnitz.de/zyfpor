/*
 * APSOC_CV
 * apsoc_cv_main.h
 *
 * Author		:	Murali Padmanabha
 * E-Mail		:	murali.padmanabha@s2013.tu-chemnitz.de
 * Company		:	Technische Universität Chemnitz
 * 
 * Created on	:	Apr 23, 2015
 */

#ifndef APSOC_CV_MAIN_H_
#define APSOC_CV_MAIN_H_

#include <stdio.h>
#include <unistd.h>
#include <time.h>
#include <iomanip>

#include "apsoc_cv_apps.h"

#define APP_NAME "APSOC Based Computer Vision Evaluation Platform"
/*Macro definitions */

/*Function definitions*/







#endif /* APSOC_CV_MAIN_H_ */
