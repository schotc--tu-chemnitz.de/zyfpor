/*
 * APSOC_CV
 * apsoc_cv_apps.cpp
 *
 * Author		:	Murali Padmanabha; Christian Schott
 * E-Mail		:	murali.padmanabha@s2013.tu-chemnitz.de; christian.schott@etit.tu-chemnitz.de
 * Company		:	Technische Universität Chemnitz
 * 
 * Created on	:	May 1, 2015
 */

#include "apsoc_cv_apps.h"

cv::VideoCapture vCap;

cv::Mat frameBuffer[FRAME_SW_BUFF_SIZE];

struct st_lines{
	Point point1;
	Point point2;
};


unsigned int currFrameIndex=0;
unsigned int memCopy=0;

// function prototypes
void *captureVideoFrames(void *);
char getLineISCT(float p0x, float p0y, float p1x, float p1y, float p2x, float p2y,
				 float p3x, float p3y, float *isct_x, float *isct_y);

void ApsocCvApp_HW_Filter(unsigned int vidSrc){

	// Initialize the hardware modules
	VideoStreamVDMA_Configure();
	ImageFilter_Configure();
	usleep(10000);
	VideoStreamVDMA_Init();

	//Start the camera device
	if(vidSrc == CAMERA_STREAM)
	{
		Camera_Open(&vCap, VIDEO_DEVICE_ID);
		Camera_Set_Prop(&vCap);		
		Camera_Get_Prop(&vCap);		
	}
	else
		Camera_Open(&vCap, VID_PATH);
		
	//Start Application
	//Camera_Get_Prop(&vCap);

	//Show device status
	//VideoStreamVDMA_Show_Status();
	//ImageFilter_Show_Status();

	// Initialize frame objects for receiving and sending image frames
	cvNamedWindow( "Source Stream", CV_WINDOW_AUTOSIZE );
	cvNamedWindow( "Processed Stream", CV_WINDOW_AUTOSIZE );
	cvStartWindowThread();
	cv::Mat capFrame;
	cv::Mat inFrame((int)IN_FRAME_HEIGHT,(int)IN_FRAME_WIDTH,CV_8UC3 );
	//inFrame.data=streamDevHandler.mem2VdmaVirtAddr;
	cv::Mat outFrame((int)OUT_FRAME_HEIGHT,(int)OUT_FRAME_WIDTH,CV_8UC1);
	//outFrame.data=streamDevHandler.vdma2MemVirtAddr;
	int from_to[] = { 0,0, 1,1, 2,2 };// used for channel conversion from 3 to 4 channel

	int64_t ticks1,ticks2,ticks3,ticks4,ticks5;
	double captureTicks, preProcessTicks, processTicks,postProcessTicks,totalTicks,tickFreq;

        int i = 0;
	while(1){
                i++;
                cout << i << endl;
		ticks1 =(double)cv::getTickCount();
		tickFreq = cv::getTickFrequency();
		vCap >> inFrame;

		ticks2 =(double)cv::getTickCount();
		// Convert 3 channel image to a 4 channel image to match the 32 bit width interface of the image filter
		//cv::mixChannels(&capFrame,1,&inFrame,1,from_to,3);
		memcpy((unsigned char*)streamDevHandler.mem2VdmaVirtAddr, (unsigned char*)inFrame.data, (IN_FRAME_HEIGHT*IN_FRAME_WIDTH*sizeof(unsigned char)*IN_BYTES_PER_PIXEL));
		imshow("Source Stream",inFrame);
		ticks3 = (double)cv::getTickCount();
		ImageFilter_Convert();
		ticks4 = (double)cv::getTickCount();
		memcpy((unsigned char*)outFrame.data, (unsigned char*)streamDevHandler.vdma2MemVirtAddr, (OUT_FRAME_HEIGHT*OUT_FRAME_WIDTH*sizeof(unsigned char)*OUT_BYTES_PER_PIXEL));
		imshow("Processed Stream",outFrame);
		ticks5 = (double)cv::getTickCount();
		//compute statistics
		captureTicks = (ticks2 - ticks1) / tickFreq;
		preProcessTicks = (ticks3 - ticks2) / tickFreq;
		processTicks = (ticks4 - ticks3) / tickFreq;
		postProcessTicks = (ticks5 - ticks4) / tickFreq;
		totalTicks = (ticks5 - ticks1) / tickFreq;

		//Print Statistics
		std::cout<<"\e[1J"; //Erases the screen
		std::cout<<"Executing Hardware Filter"<<std::endl;
		std::cout<<"Frame capture time in ms:\t\t\t"<<captureTicks*1000<<std::endl;
		std::cout<<"Frame Pre-Process and transfer time in ms:\t"<<preProcessTicks*1000<<std::endl;
		std::cout<<"Frame Process time in ms:\t\t\t"<<processTicks*1000<<std::endl;
		std::cout<<"Frame Post-Process and transfer time in ms:\t"<<postProcessTicks*1000<<std::endl;
		std::cout<<"Total frame processing time in ms:\t\t"<<totalTicks*1000<<std::endl;
		std::cout<<"Achieved frame rate in FPS:\t\t\t"<<1/totalTicks<<std::endl;

		char c = cvWaitKey(1);
		// Terminate application on pressing ESC key
		if ( c == 27 )
			break;
	}
	//Close the windows
	cv::destroyAllWindows();
	cvWaitKey(1);

	// Stop the hardware modules
	VideoStreamVDMA_Stop();
	//Deconfigure devices and release the memory
	VideoStreamVDMA_Deconfigure();
	ImageFilter_Deconfigure();
	//Close the source
	if(vidSrc ==CAMERA_STREAM)
		Camera_Close(&vCap, VIDEO_DEVICE_ID);
	else
		Camera_Close(&vCap, VID_PATH);

}

void ApsocCvApp_SW_Canny(unsigned int vidSrc){
	//Start the camera device
	if(vidSrc ==CAMERA_STREAM)
		Camera_Open(&vCap, VIDEO_DEVICE_ID);
	else
		Camera_Open(&vCap, VID_PATH);

	//Start Application
	cvNamedWindow( "Source Stream", CV_WINDOW_AUTOSIZE );
	cvNamedWindow( "Processed Stream", CV_WINDOW_AUTOSIZE );
	cv::Mat inFrame;
	cv::Mat outFrame;

	double ticks1,ticks2,ticks3,ticks4,ticks5;
	double captureTicks, preProcessTicks, processTicks,postProcessTicks,totalTicks,tickFreq;

	while(1){
		ticks1 = (double)cv::getTickCount();
		tickFreq = cv::getTickFrequency();
		vCap.read(inFrame);
		ticks2 = (double)cv::getTickCount();
		cvtColor(inFrame, outFrame, CV_BGR2GRAY);
		ticks3 = (double)cv::getTickCount();
		GaussianBlur(outFrame, outFrame, Size(7,7), 1.5, 1.5);
		Canny(outFrame, outFrame, 0, 30, 3);
		ticks4 = (double)cv::getTickCount();
		imshow("Source Stream",inFrame);
		imshow("Processed Stream",outFrame);
		ticks5 = (double)cv::getTickCount();

		//compute statistics
		captureTicks = (ticks2 - ticks1) / tickFreq;
		preProcessTicks = (ticks3 - ticks2) / tickFreq;
		processTicks = (ticks4 - ticks3) / tickFreq;
		postProcessTicks = (ticks5 - ticks4) / tickFreq;
		totalTicks = (ticks5 - ticks1) / tickFreq;

		//Print Statistics
		std::cout<<"\e[1J"; //Erases the screen
		std::cout<<"Executing Canny Edge Detection SW Filter"<<std::endl;
		std::cout<<"Frame capture time in ms:\t\t\t"<<captureTicks*1000<<std::endl;
		std::cout<<"Frame Pre-Process and transfer time in ms:\t"<<preProcessTicks*1000<<std::endl;
		std::cout<<"Frame Process time in ms:\t\t\t"<<processTicks*1000<<std::endl;
		std::cout<<"Frame Post-Process and transfer time in ms:\t"<<postProcessTicks*1000<<std::endl;
		std::cout<<"Total frame processing time in ms:\t\t"<<totalTicks*1000<<std::endl;
		std::cout<<"Achieved frame rate in FPS:\t\t\t"<<1/totalTicks<<std::endl;
		char c = cvWaitKey(1);
		if ( c == 27 ) break;
	}
	//Close the windows
	cv::destroyAllWindows();
	//Close the source
	if(vidSrc ==CAMERA_STREAM)
		Camera_Close(&vCap, VIDEO_DEVICE_ID);
	else
		Camera_Close(&vCap, VID_PATH);
}

void ApsocCvApp_SW_SobelXY(unsigned int vidSrc){
	//Start the camera device
	if(vidSrc ==CAMERA_STREAM)
		Camera_Open(&vCap, VIDEO_DEVICE_ID);		
	else
		Camera_Open(&vCap, VID_PATH);
		
	//Start Application
	cvNamedWindow( "Source Stream", CV_WINDOW_AUTOSIZE );
	cvNamedWindow( "Processed Stream", CV_WINDOW_AUTOSIZE );
	cv::Mat inFrame;
	cv::Mat outFrame;

	cv::Mat gray;
	int scale = 1;
	int delta = 0;
	int ddepth = CV_16S;

	double ticks1,ticks2,ticks3,ticks4,ticks5;
	double captureTicks, preProcessTicks, processTicks,postProcessTicks,totalTicks,tickFreq;

	while(1){
		ticks1 = (double)cv::getTickCount();
		tickFreq = cv::getTickFrequency();
		vCap.read(inFrame);
		ticks2 = (double)cv::getTickCount();
		ticks3 = (double)cv::getTickCount();
		GaussianBlur( inFrame, inFrame, Size(3,3), 0, 0, BORDER_DEFAULT );
		/// Convert it to gray
		cvtColor( inFrame, gray, CV_RGB2GRAY );
		/// Generate grad_x and grad_y
		Mat grad_x, grad_y;
		Mat abs_grad_x, abs_grad_y;
		/// Gradient X
		Sobel( gray, grad_x, ddepth, 1, 0, 3, scale, delta, BORDER_DEFAULT );
		convertScaleAbs( grad_x, abs_grad_x );
		/// Gradient Y
		Sobel( gray, grad_y, ddepth, 0, 1, 3, scale, delta, BORDER_DEFAULT );
		convertScaleAbs( grad_y, abs_grad_y );
		/// Total Gradient (approximate)
		addWeighted( abs_grad_x, 0.5, abs_grad_y, 0.5, 0, outFrame );
		ticks4 = (double)cv::getTickCount();
		imshow("Source Stream",inFrame);
		imshow("Processed Stream",outFrame);
		ticks5 = (double)cv::getTickCount();

		//compute statistics
		captureTicks = (ticks2 - ticks1) / tickFreq;
		preProcessTicks = (ticks3 - ticks2) / tickFreq;
		processTicks = (ticks4 - ticks3) / tickFreq;
		postProcessTicks = (ticks5 - ticks4) / tickFreq;
		totalTicks = (ticks5 - ticks1) / tickFreq;

		//Print Statistics
		std::cout<<"\e[1J"; //Erases the screen
		std::cout<<"Executing Sobel XY SW Filter"<<std::endl;
		std::cout<<"Frame capture time in ms:\t\t\t"<<captureTicks*1000<<std::endl;
		std::cout<<"Frame Pre-Process and transfer time in ms:\t"<<preProcessTicks*1000<<std::endl;
		std::cout<<"Frame Process time in ms:\t\t\t"<<processTicks*1000<<std::endl;
		std::cout<<"Frame Post-Process and transfer time in ms:\t"<<postProcessTicks*1000<<std::endl;
		std::cout<<"Total frame processing time in ms:\t\t"<<totalTicks*1000<<std::endl;
		std::cout<<"Achieved frame rate in FPS:\t\t\t"<<1/totalTicks<<std::endl;
		char c = cvWaitKey(1);
		if ( c == 27 ) break;
	}
	//Close the windows
	cv::destroyAllWindows();
	//Close the source
	if(vidSrc ==CAMERA_STREAM)
		Camera_Close(&vCap, VIDEO_DEVICE_ID);
	else
		Camera_Close(&vCap, VID_PATH);
}

void ApsocCvApp_SW_SobelX(unsigned int vidSrc){

	//Start the camera device
	if(vidSrc ==CAMERA_STREAM)
		Camera_Open(&vCap, VIDEO_DEVICE_ID);
	else
		Camera_Open(&vCap, VID_PATH);
		
	// Initialize frame objects for receiving and sending image frames
	cvNamedWindow( "Source Stream", CV_WINDOW_AUTOSIZE );
	cvNamedWindow( "Processed Stream", CV_WINDOW_AUTOSIZE );
	cvStartWindowThread();
	cv::Mat capFrame;
	cv::Mat inFrame((int)IN_FRAME_HEIGHT,(int)IN_FRAME_WIDTH,CV_8UC3 );
	cv::Mat outFrame((int)OUT_FRAME_HEIGHT,(int)OUT_FRAME_WIDTH,CV_8UC3);

	// Additional definitions
	cv::Scalar pix(30,30,30);
	int scale = 1;
	int delta = 0;
	int ddepth = CV_16S;
	Mat element = getStructuringElement( MORPH_RECT,Size( 2*1 + 1, 2*1+1),Point( 1, 1 ) );

	double ticks1,ticks2,ticks3,ticks4,ticks5;
	double captureTicks, preProcessTicks, processTicks,postProcessTicks,totalTicks,tickFreq;

	while(1){
		ticks1 = (double)cv::getTickCount();
		tickFreq = cv::getTickFrequency();
		vCap >> capFrame;
		ticks2 = (double)cv::getTickCount();
		ticks3 = (double)cv::getTickCount();
		//Start of algorithm
		cv::Sobel( capFrame, inFrame, ddepth, 1, 0, 3, scale, delta, BORDER_DEFAULT );
		cv::subtract(inFrame,pix,inFrame);
		cv::convertScaleAbs(inFrame,outFrame,1,0);
		cv::erode(outFrame,outFrame,element);
		cv::dilate(outFrame,outFrame,element);
		// End of algorithm
		ticks4 = (double)cv::getTickCount();
		imshow("Source Stream",inFrame);
		imshow("Processed Stream",outFrame);
		ticks5 = (double)cv::getTickCount();

		//compute statistics
		captureTicks = (ticks2 - ticks1) / tickFreq;
		preProcessTicks = (ticks3 - ticks2) / tickFreq;
		processTicks = (ticks4 - ticks3) / tickFreq;
		postProcessTicks = (ticks5 - ticks4) / tickFreq;
		totalTicks = (ticks5 - ticks1) / tickFreq;

		//Print Statistics
		std::cout<<"\e[1J"; //Erases the screen
		std::cout<<"Executing Sobel X SW Filter"<<std::endl;
		std::cout<<"Frame capture time in ms:\t\t\t"<<captureTicks*1000<<std::endl;
		std::cout<<"Frame Pre-Process and transfer time in ms:\t"<<preProcessTicks*1000<<std::endl;
		std::cout<<"Frame Process time in ms:\t\t\t"<<processTicks*1000<<std::endl;
		std::cout<<"Frame Post-Process and transfer time in ms:\t"<<postProcessTicks*1000<<std::endl;
		std::cout<<"Total frame processing time in ms:\t\t"<<totalTicks*1000<<std::endl;
		std::cout<<"Achieved frame rate in FPS:\t\t\t"<<1/totalTicks<<std::endl;

		char c = cvWaitKey(1);
		// Terminate application on pressing ESC key
		if ( c == 27 )
			break;
	}
	//Close the windows
	cv::destroyAllWindows();
	//Close the source
	if(vidSrc ==CAMERA_STREAM)
		Camera_Close(&vCap, VIDEO_DEVICE_ID);
	else
		Camera_Close(&vCap, VID_PATH);
}

void ApsocCvApp_SW_FrameGrab(unsigned int vidSrc){

	//Start the camera device
	if(vidSrc ==CAMERA_STREAM)
		Camera_Open(&vCap, VIDEO_DEVICE_ID);
	else
		Camera_Open(&vCap, VID_PATH);
		
	// Initialize frame objects for receiving and sending image frames
	cvNamedWindow( "Source Stream", CV_WINDOW_AUTOSIZE );
	cvStartWindowThread();
	cv::Mat capFrame;

	double ticks1,ticks2,ticks3,ticks4,ticks5;
	double captureTicks, preProcessTicks, processTicks,postProcessTicks,totalTicks,tickFreq;

	while(1){
		ticks1 = (double)cv::getTickCount();
		tickFreq = cv::getTickFrequency();
		vCap >> capFrame;
		ticks2 = (double)cv::getTickCount();
		ticks3 = (double)cv::getTickCount();
		//Start of algorithm

		// End of algorithm
		ticks4 = (double)cv::getTickCount();
		imshow("Source Stream",capFrame);
		ticks5 = (double)cv::getTickCount();

		//compute statistics
		captureTicks = (ticks2 - ticks1) / tickFreq;
		preProcessTicks = (ticks3 - ticks2) / tickFreq;
		processTicks = (ticks4 - ticks3) / tickFreq;
		postProcessTicks = (ticks5 - ticks4) / tickFreq;
		totalTicks = (ticks5 - ticks1) / tickFreq;

		//Print Statistics
		std::cout<<"\e[1J"; //Erases the screen
		std::cout<<"Executing Frame Grabbing"<<std::endl;
		std::cout<<"Frame capture time in ms:\t\t\t"<<captureTicks*1000<<std::endl;
		std::cout<<"Frame Pre-Process and transfer time in ms:\t"<<preProcessTicks*1000<<std::endl;
		std::cout<<"Frame Process time in ms:\t\t\t"<<processTicks*1000<<std::endl;
		std::cout<<"Frame Post-Process and transfer time in ms:\t"<<postProcessTicks*1000<<std::endl;
		std::cout<<"Total frame processing time in ms:\t\t"<<totalTicks*1000<<std::endl;
		std::cout<<"Achieved frame rate in FPS:\t\t\t"<<1/totalTicks<<std::endl;

		char c = cvWaitKey(1);
		// Terminate application on pressing ESC key
		if ( c == 27 )
			break;
	}
	//Close the windows
	cv::destroyAllWindows();
	//Close the source
	if(vidSrc ==CAMERA_STREAM)
		Camera_Close(&vCap, VIDEO_DEVICE_ID);
	else
		Camera_Close(&vCap, VID_PATH);
}

void ApsocCvApp_HW_FilterOpt(unsigned int vidSrc){

	// Initialize the hardware modules
	VideoStreamVDMA_Configure();
	ImageFilter_Configure();
	usleep(10000);
	VideoStreamVDMA_Init();

	//Start the camera device
	if(vidSrc ==CAMERA_STREAM)
	{
		//Start Application
		Camera_Open(&vCap, VIDEO_DEVICE_ID);
		Camera_Set_Prop(&vCap);		
		Camera_Get_Prop(&vCap);
	}
	else
		Camera_Open(&vCap, VID_PATH);

	HoughLineValPair houghLineValPair[HOUGH_LINE_RHO_ANGLE_VALUES_MAX];

	//Show device status
	//VideoStreamVDMA_Show_Status();
	//ImageFilter_Show_Status();

	// Initialize frame objects for receiving and sending image frames
	//cvNamedWindow("Source Stream", CV_WINDOW_AUTOSIZE);
	//cvNamedWindow("Processed Stream", CV_WINDOW_AUTOSIZE);
	//cvNamedWindow("Test Frame", CV_WINDOW_AUTOSIZE);
	//cvNamedWindow("Small Image Left", CV_WINDOW_AUTOSIZE);
	//cvNamedWindow("Small Image Right", CV_WINDOW_AUTOSIZE);
	cvNamedWindow("Match Image Left", CV_WINDOW_AUTOSIZE);
	cvNamedWindow("Match Image Right", CV_WINDOW_AUTOSIZE);
	cvNamedWindow("Result Frame", CV_WINDOW_AUTOSIZE);
	cvStartWindowThread();

	cv::Mat capFrame;
	cv::Mat inFrame((int)IN_FRAME_HEIGHT,(int)IN_FRAME_WIDTH,IN_FRAME_CHANNELS );
	cv::Mat outFrame((int)OUT_FRAME_HEIGHT,(int)OUT_FRAME_WIDTH,OUT_FRAME_CHANNELS);
	int from_to[] = { 0,0, 1,1, 2,2 };// used for channel conversion from 3 to 4 channel
	
	unsigned int prevFrameIndex, frameCnt;
	double ticks1,ticks2,ticks3,ticks4,ticks5;
	double captureTicks, preProcessTicks, processTicks,postProcessTicks,totalTicks,tickFreq;
	double minTotalTicks=0xffff, maxTotalTicks=0, avgFramesRate,sumFramesRate ;
	
	cv::Mat testFrame((int)OUT_FRAME_HEIGHT,(int)OUT_FRAME_WIDTH,CV_8UC3);
	cv::Mat resultFrame;
	cv::Mat tmpFrame;
	cv::Mat small_image_left;
	cv::Mat small_image_right;
	cv::Mat origin_img;
	cv::Mat descriptor_origin_img;
	cv::Mat descriptor_left_img;
	cv::Mat descriptor_right_img;
	cv::Mat match_img_left;
	cv::Mat match_img_right;
	st_lines tmp_st_lines1;
	st_lines tmp_st_lines2;
	vector<st_lines> v_lines_isct;
	vector<Point> v_isct_points;
	Point isct_tmp;
	float isct_x_tmp, isct_y_tmp;
	Point isct_avg;
	
	unsigned int small_window_width, small_window_height;
	
	// Paarameters for ORB detector
	int nfeatures = 1000;
	float scaleFactor = 1.8f;
	int nlevels = 4;
	int edgeThreshold = 31;
	int firstLevel = 0;
	int WTA_K = 3;
	int scoreType = ORB::HARRIS_SCORE;
	int patchSize = 14;
	
	cv::OrbFeatureDetector det_orb = cv::ORB(nfeatures, scaleFactor, nlevels, edgeThreshold, firstLevel,
														WTA_K, scoreType, patchSize);
	cv::OrbDescriptorExtractor ext_orb;
	vector<cv::KeyPoint> v_keypts_left_img;
	vector<cv::KeyPoint> v_keypts_right_img;
	vector<cv::KeyPoint> v_keypts_origin_img;
	Point mean_keypt_left;
	Point mean_keypt_right;
	
	// Matcher
	cv::BFMatcher matcher(NORM_HAMMING2, true);
	vector<cv::DMatch> v_matches_left;
	vector<cv::DMatch> v_matches_right;
	vector<cv::DMatch> v_good_matches_left;
	vector<cv::DMatch> v_good_matches_right;
    
    origin_img = imread("../../testimages/doorhandle2.jpg", CV_LOAD_IMAGE_GRAYSCALE);
    cv::resize(origin_img, origin_img, Size(origin_img.cols/4, origin_img.rows/4));
    
    // detect and extract keypoints from origin image
    det_orb.detect(origin_img, v_keypts_origin_img);
    ext_orb.compute(origin_img, v_keypts_origin_img, descriptor_origin_img);
    
    // save output video as file
    //cv::VideoWriter outputVideo("ZYFPOR-outputVideo.mpeg", CV_FOURCC('M','P','E','G'), 20, cv::Size(960, 540), true);
    //cv::VideoWriter leftoutputVideo("ZYFPOR-leftoutputVideo.mpeg", CV_FOURCC('M','P','E','G'), 15, cv::Size(484, 324), true);
    cv::VideoWriter rightoutputVideo("ZYFPOR-rightoutputVideo.mpeg", CV_FOURCC('M','P','E','G'), 15, cv::Size(484, 324), true);
    	
	//Start background frame capture engine
	pthread_t captureVideoThread;
	int rc1;
	 if( (rc1=pthread_create( &captureVideoThread, NULL, &captureVideoFrames, NULL)) )
	  {
	     std::cout<<"Thread creation failed: %d\n"<<rc1<<std::endl;
	  }

	// Wait until Capture thread has captured atleast frames equal to the lag   
	while (currFrameIndex <= FRAME_SW_BUFF_LAG);
	while(1){
		ticks1 = (double)cv::getTickCount();
		tickFreq = cv::getTickFrequency();

		//Copy frame data from the buffered memory to the local frame Mat
		if(currFrameIndex < FRAME_SW_BUFF_LAG)
			prevFrameIndex=FRAME_SW_BUFF_SIZE - FRAME_SW_BUFF_LAG + currFrameIndex;
		else
			prevFrameIndex= currFrameIndex -FRAME_SW_BUFF_LAG;

		ticks2 = (double)cv::getTickCount();
		// Convert 3 channel image to a 4 channel image to match the 32 bit width interface of the image filter
		//cv::mixChannels(&frameBuffer[prevFrameIndex],1,&inFrame,1,from_to,3);
		memcpy((unsigned char*)streamDevHandler.mem2VdmaVirtAddr, (unsigned char*)frameBuffer[prevFrameIndex].datastart, (IN_FRAME_HEIGHT*IN_FRAME_WIDTH*sizeof(unsigned char)*IN_BYTES_PER_PIXEL));
		ticks3 = (double)cv::getTickCount();
		ImageFilter_Convert();
		
		unsigned int cnt = ImageFilter_Read_RhoAnglePair(houghLineValPair);
		
/*		for ( unsigned int i=0; i<cnt; i++ )
		{
			//std::cout<<houghLineValPair[i].rhoValue<<"\t"<<houghLineValPair[i].angleValue<<std::endl;
			std::cout<<houghLineValPair[i].rhoValue.to_float()<<"\t"<<houghLineValPair[i].angleValue.to_float()<<std::endl;
		}
*/		
		
		ticks4 = (double)cv::getTickCount();
		memcpy((unsigned char*)outFrame.datastart, (unsigned char*)streamDevHandler.vdma2MemVirtAddr, (OUT_FRAME_HEIGHT*OUT_FRAME_WIDTH*sizeof(unsigned char)*OUT_BYTES_PER_PIXEL));
	    //imshow("Source Stream",frameBuffer[prevFrameIndex]);
		
		cvtColor(outFrame, testFrame, CV_GRAY2RGB);
				
		/* further image processing, now on Zynq PS
		 * 
		 *  1. read out houghlines values from HW
		 *  2. calculate intersection of "floor lines"
		 *  3. use average of intersections to determine ROI for ORB
		 *  4. ORB processing on current frame
		 *  5. matching
		 * 
		 */
		// read out houghlines values
		for(unsigned short i = 0; i < cnt; i++)
		{
			float rho = houghLineValPair[i].rhoValue.to_float();
			float theta = houghLineValPair[i].angleValue.to_float();
			
			// calculate points for drawing detected lines
			Point pt1, pt2;
			double a = cos(theta);
			double b = sin(theta);
			double x0 = a*rho;
			double y0 = b*rho;
			pt1.x = cvRound(x0 + 1000*(-b));
			pt1.y = cvRound(y0 + 1000*(a));
			pt2.x = cvRound(x0 - 1000*(-b));
			pt2.y = cvRound(y0 - 1000*(a));
						
			// store important lines into vector			
			if( (theta >= 0.3) && (theta <= 2.8) )
			{
				tmp_st_lines1.point1 = pt1;
				tmp_st_lines1.point2 = pt2;
				v_lines_isct.push_back(tmp_st_lines1);
			}
				
			// draw lines
			if( (theta >= 0.3) && (theta <= 2.8) )
				line(testFrame, pt1, pt2, Scalar(255,0,0), 3, 8, 0);			
		}
		
		// calculation of intersection of "floor lines"
		for(unsigned int i = 0; i < v_lines_isct.size(); i++)
		{
			for(unsigned int j = 0; j < v_lines_isct.size(); j++)
			{
				tmp_st_lines1 = v_lines_isct.at(i);		// first line
				tmp_st_lines2 = v_lines_isct.at(j);		// every other line
				
				if(	getLineISCT((float) tmp_st_lines1.point1.x, (float) tmp_st_lines1.point1.y,
								(float) tmp_st_lines1.point2.x, (float) tmp_st_lines1.point2.y,
								(float) tmp_st_lines2.point1.x, (float) tmp_st_lines2.point1.y,
								(float) tmp_st_lines2.point2.x, (float) tmp_st_lines2.point2.y,
								&isct_x_tmp, &isct_y_tmp) )
				{
					isct_tmp.x = isct_x_tmp;
					isct_tmp.y = isct_y_tmp;
					v_isct_points.push_back(isct_tmp);
				}
			}
		}
		
		// draw intersections into image
		for(unsigned int i = 0; i < v_isct_points.size(); i++)
		{
			line(testFrame, v_isct_points.at(i), v_isct_points.at(i), Scalar(0,0,255), 3, 8, 0);
		}
		
		// calculate the average of the intersections
		for(unsigned int i = 0; i < v_isct_points.size(); i++)
		{
			isct_tmp = v_isct_points.at(i);
			isct_avg.x += abs(isct_tmp.x);
			isct_avg.y += abs(isct_tmp.y);
		}
		if(v_isct_points.size() != 0)
		{
			isct_avg.x /= v_isct_points.size();
			isct_avg.y /= v_isct_points.size();
		}
		// draw average intersection
		circle(testFrame, isct_avg, 10, Scalar(0,0,255), 1, 8, 0);
		//line(testFrame, isct_avg, isct_avg, Scalar(0,255,255), 3, 8, 0);
		
		// create search window using the average intersection point
		if( (isct_avg.x != 0) && (isct_avg.x <= outFrame.cols) && (isct_avg.y != 0) && (isct_avg.y <= outFrame.rows) )
		{
			small_window_width = outFrame.cols/3;
			small_window_height = 0.6*outFrame.rows;
			if( (isct_avg.y + 0.3*outFrame.rows) > outFrame.rows )
				small_window_height = abs(outFrame.rows - isct_avg.y);
			else
				small_window_height = 0.6*outFrame.rows;
			// using image from HW
			//small_image_left = cv::Mat(outFrame, cv::Rect(0, abs(isct_avg.y-(0.35*outFrame.rows)), small_window_width, small_window_height)).clone();
			//small_image_right = cv::Mat(outFrame, cv::Rect((outFrame.cols*2)/3, abs(isct_avg.y-(0.35*outFrame.rows)), small_window_width, small_window_height)).clone();
			// using source image
			small_image_left = cv::Mat(frameBuffer[prevFrameIndex], cv::Rect(0, abs(isct_avg.y-(0.3*outFrame.rows)), small_window_width, small_window_height)).clone();
			small_image_right = cv::Mat(frameBuffer[prevFrameIndex], cv::Rect((outFrame.cols*2)/3, abs(isct_avg.y-(0.3*outFrame.rows)), small_window_width, small_window_height)).clone();
			
			// fixed search window
			//small_image_left = cv::Mat(frameBuffer[prevFrameIndex], cv::Rect(0, 0, small_window_width, small_window_height)).clone();
			//small_image_right = cv::Mat(frameBuffer[prevFrameIndex], cv::Rect((frameBuffer[prevFrameIndex].cols*2)/3, 0, small_window_width, small_window_height)).clone();
		}
		
		// ORB processing on current frame
		det_orb.detect(small_image_left, v_keypts_left_img);
		ext_orb.compute(small_image_left, v_keypts_left_img, descriptor_left_img);
		det_orb.detect(small_image_right, v_keypts_right_img);
		ext_orb.compute(small_image_right, v_keypts_right_img, descriptor_right_img);
		
		// calculate mean of keypoints
		for(unsigned int i = 0; i < v_keypts_left_img.size(); i++)
		{
			mean_keypt_left.x += v_keypts_left_img[i].pt.x;
			mean_keypt_left.y += v_keypts_left_img[i].pt.y;
		}
		if(v_keypts_left_img.size() != 0)
		{
			mean_keypt_left.x /= v_keypts_left_img.size();
			mean_keypt_left.y /= v_keypts_left_img.size();
		}
		
		for(unsigned int i = 0; i < v_keypts_right_img.size(); i++)
		{
			mean_keypt_right.x += v_keypts_right_img[i].pt.x;
			mean_keypt_right.y += v_keypts_right_img[i].pt.y;
			circle(small_image_left, mean_keypt_left, 10, Scalar(0,255,255), 1, 8, 0);
		}
		if(v_keypts_right_img.size() != 0)
		{
			mean_keypt_right.x /= v_keypts_right_img.size();
			mean_keypt_right.y /= v_keypts_right_img.size();
			circle(small_image_right, mean_keypt_right, 10, Scalar(0,255,255), 1, 8, 0);
		}
					
		// matching
		matcher.match(descriptor_left_img, descriptor_origin_img, v_matches_left);
		matcher.match(descriptor_right_img, descriptor_origin_img, v_matches_right);
		
		// distance thresholding, due to ORB paper from willowgarage
		unsigned int dist_threshold = 64;
		for(unsigned int i = 0; i < v_matches_left.size(); i++)
		{
			if((v_matches_left[i].distance > 0) &&(v_matches_left[i].distance < dist_threshold))
				v_good_matches_left.push_back(v_matches_left[i]);
		}
		for(unsigned int i = 0; i < v_matches_right.size(); i++)
		{
			if((v_matches_right[i].distance > 0) &&(v_matches_right[i].distance < dist_threshold))
				v_good_matches_right.push_back(v_matches_right[i]);
		}
		
//		cv::drawMatches(small_image_left, v_keypts_left_img, origin_img, v_keypts_origin_img, v_matches_left, match_img_left);
//		cv::drawMatches(small_image_right, v_keypts_right_img, origin_img, v_keypts_origin_img, v_matches_right, match_img_right);
		
		cv::drawMatches(small_image_left, v_keypts_left_img, origin_img, v_keypts_origin_img, v_good_matches_left, match_img_left);
		cv::drawMatches(small_image_right, v_keypts_right_img, origin_img, v_keypts_origin_img, v_good_matches_right, match_img_right);
			
		resultFrame = testFrame.clone();	
			
		tmpFrame = Mat(resultFrame, Rect(0, 0.2*resultFrame.rows, small_window_width, small_window_height));
		small_image_left.copyTo(tmpFrame);
		//match_img_left.copyTo(tmpFrame);
		tmpFrame = Mat(resultFrame, Rect((resultFrame.cols*2)/3, 0.2*resultFrame.rows, small_window_width, small_window_height));
		small_image_right.copyTo(tmpFrame);		 
		//match_img_right.copyTo(tmpFrame);
				 
				 
		// clear vectors after processing of frame
		v_lines_isct.clear();
		v_isct_points.clear();
		v_keypts_left_img.clear();
		v_keypts_right_img.clear();
		v_matches_left.clear();
		v_matches_right.clear();
		v_good_matches_left.clear();
		v_good_matches_right.clear();
		

		//imshow("Test Frame", testFrame);
		//imshow("Small Image Left", small_image_left);
		//imshow("Small Image Right", small_image_right);
		if(!(match_img_left.empty()))
			imshow("Match Image Left", match_img_left);
		if(!(match_img_right.empty()))
			imshow("Match Image Right", match_img_right);
		//imshow("Processed Stream",outFrame);
		imshow("Result Frame", resultFrame);
		
		// write output video to file
		//outputVideo.write(resultFrame);
		//leftoutputVideo.write(match_img_left);
		rightoutputVideo.write(match_img_right);
		
		ticks5 = (double)cv::getTickCount();
		
		//compute statistics		
		totalTicks = (ticks5 - ticks1) / tickFreq;
		if (totalTicks < minTotalTicks )
			minTotalTicks = totalTicks;
		if (totalTicks > maxTotalTicks)
			maxTotalTicks = totalTicks;	
		if (frameCnt <= 30){
			frameCnt+=1;
			sumFramesRate+= 1/totalTicks;
		}
		else {
			avgFramesRate=sumFramesRate/30;
			sumFramesRate=0;
			frameCnt=0;
			captureTicks = (ticks2 - ticks1) / tickFreq;
			preProcessTicks = (ticks3 - ticks2) / tickFreq;
			processTicks = (ticks4 - ticks3) / tickFreq;
			postProcessTicks = (ticks5 - ticks4) / tickFreq;
			//Print Statistics
			std::cout<<"\e[1J"; //Erases the screen
			std::cout<<"Executing Hardware Filter"<<std::endl;
			std::cout<<"Frame capture time in ms:\t\t\t"<<captureTicks*1000<<std::endl;
			std::cout<<"Frame Pre-Process and transfer time in ms:\t"<<preProcessTicks*1000<<std::endl;
			std::cout<<"Frame Process time in ms:\t\t\t"<<processTicks*1000<<std::endl;
			std::cout<<"Frame Post-Process and transfer time in ms:\t"<<postProcessTicks*1000<<std::endl;
			std::cout<<"Total frame processing time in ms:\t\t"<<totalTicks*1000<<std::endl;
			std::cout<<"Achieved Current frame rate in FPS:\t\t"<<1/totalTicks<<std::endl;
			std::cout<<"Achieved Max frame rate in FPS:\t\t\t"<<1/minTotalTicks<<std::endl;
			std::cout<<"Achieved Min frame rate in FPS:\t\t\t"<<1/maxTotalTicks<<std::endl;
			std::cout<<"Achieved Avg frame rate in FPS:\t\t\t"<<avgFramesRate<<std::endl;
			
		}
		char c = cvWaitKey(1);
		// Terminate application on pressing ESC key
		if ( c == 27 )
			break;
	}
	//Close the windows
	cv::destroyAllWindows();
	cvWaitKey(1);

	// Stop the hardware modules
	VideoStreamVDMA_Stop();
	//Deconfigure devices and release the memory
	VideoStreamVDMA_Deconfigure();
	ImageFilter_Deconfigure();
	//Close the source
	if(vidSrc ==CAMERA_STREAM)
		Camera_Close(&vCap, VIDEO_DEVICE_ID);
	else
		Camera_Close(&vCap, VID_PATH);

}

void *captureVideoFrames(void * args){
	while(1){
		vCap.read(frameBuffer[currFrameIndex]);
		currFrameIndex++;
		if(currFrameIndex >= FRAME_SW_BUFF_SIZE)
			currFrameIndex=0;
		usleep(50000);
	}
	return 0;
}

char getLineISCT(float p0x, float p0y, float p1x, float p1y, 
				 float p2x, float p2y, float p3x, float p3y, 
				 float *isct_x, float *isct_y)
{
	 float s1x, s1y, s2x, s2y;
	 s1x = p1x - p0x;
	 s1y = p1y - p0y;
	 s2x = p3x - p2x;
	 s2y = p3y - p2y;
	 
	 float s, t;
	 s = (-s1y * (p0x - p2x) + s1x * (p0y - p2y)) / (-s2x * s1y + s1x * s2y);
	 t =  (s2x * (p0y - p2y) - s2y * (p0x - p2x)) / (-s2x * s1y + s1x * s2y);
	 
	 if( (s>=0) && (s<=1) && (t>=0) && (t<=1) )
	 {
		 // intersection
		 if(isct_x != NULL)
			*isct_x = p0x + (t * s1x);
		 if(isct_y != NULL)
			*isct_y = p0y + (t * s1y);
		 return 1;
	 }
	 
	 return 0;		// no intersection
}		 
	
					 

