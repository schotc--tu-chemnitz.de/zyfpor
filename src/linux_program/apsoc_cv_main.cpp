/*
 * APSOC_CV
 * apsoc_cv_main.cpp
 *
 * Author		:	Murali Padmanabha
 * E-Mail		:	murali.padmanabha@s2013.tu-chemnitz.de
 * Company		:	Technische Universität Chemnitz
 * 
 * Created on	:	Apr 23, 2015
 */

#include "apsoc_cv_main.h"


int main(int argc, char *argv[]){

	char userInput;
	//Start Application
	//Create the UI
	START:
	std::cout<<"\e[1;37;44m";//set the background color to Blue and the text to white
	std::cout<<"\e[2J";//clear the screen and go to home
	std::cout<<std::setfill('-')<<std::setw(80)<<"-"<<std::endl;
	std::cout<<std::setfill('-')<<std::setw(15)<<"-"<<"\t";
	std::cout<<APP_NAME<<"\t";
	std::cout<<std::setfill('-')<<std::setw(16)<<"-"<<std::endl;
	std::cout<<std::setfill('-')<<std::setw(80)<<"-"<<std::endl;
	std::cout<<std::endl;
	std::cout<<"\t Select the application to run"<<std::endl;
	std::cout<<std::endl;
	std::cout<<"\t \t 1.Run Sobel XY GreyScale SW filter Camera"<<std::endl;
	std::cout<<std::endl;
	std::cout<<"\t \t 2.Run Sobel XY GreyScale SW filter Video File"<<std::endl;
	std::cout<<std::endl;
	std::cout<<"\t \t 3.Run Sobel X RGB SW filter Camera"<<std::endl;
	std::cout<<std::endl;
	std::cout<<"\t \t 4.Run Sobel X RGB SW filter Video File"<<std::endl;
	std::cout<<std::endl;
	std::cout<<"\t \t 5.Run Sobel HW filter Camera"<<std::endl;
	std::cout<<std::endl;
	std::cout<<"\t \t 6.Run Sobel HW filter Video File"<<std::endl;
	std::cout<<std::endl;
	std::cout<<"\t \t 7.Run Canny SW filter Camera"<<std::endl;
	std::cout<<std::endl;
	std::cout<<"\t \t 8.Run Canny SW filter Video File"<<std::endl;
	std::cout<<std::endl;
	std::cout<<"\t \t 9.Run frame grabbing Camera"<<std::endl;
	std::cout<<std::endl;
	std::cout<<"\t \t a.Run frame grabbing Video File"<<std::endl;
	std::cout<<std::endl;
	std::cout<<"\t \t b.Run Hardware filter with threads Camera"<<std::endl;
	std::cout<<std::endl;
	std::cout<<"\t \t c.Run Hardware filter with threads Video File"<<std::endl;
	std::cout<<std::endl;
	std::cout<<"Enter 1-9 or a-z to choose application or Esc to stop and X quit:"<<std::endl;
	std::cin>>userInput;
	std::cout<<"\r";
	cin.clear();
	fflush(stdin);
	while (1)
	{
		//Start selected application
		switch(userInput){
		case '1':
			ApsocCvApp_SW_SobelXY(CAMERA_STREAM);
			break;
		case '2':
			ApsocCvApp_SW_SobelXY(FILE_STREAM);
			break;
		case '3':
			ApsocCvApp_SW_SobelX(CAMERA_STREAM);
			break;
		case '4':
			ApsocCvApp_SW_SobelX(FILE_STREAM);
			break;
		case '5':
			ApsocCvApp_HW_Filter(CAMERA_STREAM);
			break;
		case '6':
			ApsocCvApp_HW_Filter(FILE_STREAM);
			break;
		case '7':
			ApsocCvApp_SW_Canny(CAMERA_STREAM);
			break;
		case '8':
			ApsocCvApp_SW_Canny(FILE_STREAM);
			break;
		case '9':
			ApsocCvApp_SW_FrameGrab(CAMERA_STREAM);
			break;
		case 'a':
			ApsocCvApp_SW_FrameGrab(FILE_STREAM);
			break;
		case 'b':
			ApsocCvApp_HW_FilterOpt(CAMERA_STREAM);
			break;
		case 'c':
			ApsocCvApp_HW_FilterOpt(FILE_STREAM);
			break;
		case 'x' :
		case 'X' :
			goto EXIT;
		default:
			goto START;
		}
		goto START;
	}

	EXIT:
	//Terminate application
	return 0;

}
