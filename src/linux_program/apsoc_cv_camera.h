/*
 * APSOC_CV
 * apsoc_cv_camera.h
 *
 * Author		:	Murali Padmanabha
 * E-Mail		:	murali.padmanabha@s2013.tu-chemnitz.de
 * Company		:	Technische Universität Chemnitz
 * 
 * Created on	:	Apr 23, 2015
 */

#ifndef APSOC_CV_CAMERA_H_
#define APSOC_CV_CAMERA_H_

#include <iostream>
#include <opencv2/opencv.hpp>
#include <opencv2/highgui/highgui.hpp>
#include "apsoc_cv_parameters.h"


using namespace cv;
using namespace std;

/*Function definitions*/
typedef struct {
	//TODO create a structure for holding the camera properties

}VideoProperty;

/*Function definitions*/
void Camera_Open(cv::VideoCapture* vCap, int);
void Camera_Open(cv::VideoCapture* vCap, const string filePath);
void Camera_Start_Capture(cv::VideoCapture* vCap);
void Camera_Get_Prop(cv::VideoCapture* vCap);
void Camera_Set_Prop(cv::VideoCapture* vCap);
void Camera_Stop_Capture(cv::VideoCapture* vCap);
void Camera_Close(cv::VideoCapture* vCap, int devId);
void Camera_Close(cv::VideoCapture* vCap, const string filePath);


#endif /* APSOC_CV_CAMERA_H_ */
