## ZYFPOR - Zynq Flexible Platform for Object Recognition

### git repository of the ZYFPOR-project - winner of the Xilinx Open Hardware Design Contest 2015 in the Zynq category

### University of Technology Chemnitz

Email:  
marcel.putsche@etit.tu-chemnitz.de  
murali.padmanabha@s2013.tu-chemnitz.de  
christian.schott@etit.tu-chemnitz.de


### Brief description of the project:

This project uses the ZYNQ-7000 All Programmable SoC for Object recognition applications. 
A Linux operating system is used with the necessary software infrastructure to fully utilize 
the interfaces on the ZedBoard. The software environment for implementing computer vision
application is built using OpenCV. The HDMI interface required for displaying the processed 
image is built using ADV7511 reference design. Object recognition algorithm is partitioned 
between the ARM using OpenCV libraries and the Programmable logic using Vivado HLS 
OpenCV libraries for better acceleration. The hardware infrastructure for streaming the video 
frames between the memory and hardware image filter IP core is designed. The performance of 
the algorithm implemented in the software and in the hardware is optimized using multi-thread support.


Board used: ZedBoard

Vivado Version: 2014.4


The repository consists of the following directories:  
/APSOC_CV - linux program with CMake build  
/image - location of the boot files, necessary for Partition 1 of the sd card  
/src - contains the source files of the linux program and the Vivado HLS project (C sources)  
--> /src/linux_program - source and header files of the linux program  
--> /src/vivado_hls - C sources of the image filter  
/vivado_hls_project/zyfpor_ip - ZYFPOR Vivado HLS project  
/vivado_project - contains a tar.xz archive of the Vivado Project  


### Instructions to build and test project

#### Hardware:  
The Vivado and Vivado HLS projects can be opened using the respective Xilinx tools (Vivado and Vivado High Level Synthesis).  

#### Software:

Step 1: Copy the files located in /image/ to partition 1 of the sd card for the ZedBoard.  
--> There must be a running linux distribution (e.g. Raspbian, Arch Linux ARM etc.) with a desktop environment on partition 2.  

Step 2: Copy the directory /APSOC_CV e.g. to the home directory of the running linux on partition 2 of the sd card.  
--> The OpenCV Library and CMake must be installed on the linux.  

Step 3: Using a terminal, change into APSOC folder.  

Step 4: Typing make will call a script building the application.  

Step 5: Launch the application with typing "sudo ./APSOC_CV"  
--> The software needs sudo rights for accessing the PL.  

Step 6: The menu of the application will show up. Typing "b" with hitting "Enter" will call the desired application.  
--> The other functions should work as well, whereas choosing to use an application that needs a video file  
--> expects a file under "../../testimages/sequences_a5100/00001_half.avi"  
--> Changing the corresponding line in the file "apsoc_cv_parameters.h" will allow to open other located videos with a different name.  
--> The resolution of the video should be 640x480 as can be seen in the mentioned file.  

### Short explanation of the files of the software application:
- apsoc_cv_apps.cpp		- contains all software application like image grabbing from a webcam or the image detection function
- apsoc_cv_apps.h		- the corresponding header file
- apsoc_cv_camera.cpp		- contains functions for accessing the webcam
- apsoc_cv_camera.h		- function prototypes
- apsoc_cv_filter.cpp		- contains functions for accessing the image processing IP core
- apsoc_cv_filter.h		- function prototypes and struct declaration
- apsoc_cv_main.cpp		- the main function of the application, handles the user input
- apsoc_cv_parameters.h		- stores all necessary parameters for the applications like resolution of input video or video file location
- apsoc_cv_vdma.cpp		- contains functions for controlling the VDMA
- apsoc_cv_vdma.h		- function prototypes and vdma register addresses/offsets
- ximage_filter_hw.h		- image processing IP core addresses

- ap_fixed_sim.h; ap_int.h; ap_int_sim.h; ap_private.h 
--> Header files for fixed-point datatype, which is used to access the data provided by the Hough Line Transform processed in the PL of the ZYNQ.


### Link to YouTube Video: http://youtu.be/508xwc262f8
 
