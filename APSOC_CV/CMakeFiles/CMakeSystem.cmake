

SET(CMAKE_SYSTEM "Linux-3.18.0-g48e414a-dirty")
SET(CMAKE_SYSTEM_NAME "Linux")
SET(CMAKE_SYSTEM_VERSION "3.18.0-g48e414a-dirty")
SET(CMAKE_SYSTEM_PROCESSOR "armv7l")

SET(CMAKE_HOST_SYSTEM "Linux-3.18.0-g48e414a-dirty")
SET(CMAKE_HOST_SYSTEM_NAME "Linux")
SET(CMAKE_HOST_SYSTEM_VERSION "3.18.0-g48e414a-dirty")
SET(CMAKE_HOST_SYSTEM_PROCESSOR "armv7l")

SET(CMAKE_CROSSCOMPILING "FALSE")

SET(CMAKE_SYSTEM_LOADED 1)
