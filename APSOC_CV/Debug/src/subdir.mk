################################################################################
# Automatically-generated file. Do not edit!
################################################################################

# Add inputs and outputs from these tool invocations to the build variables 
CPP_SRCS += \
../src/apsoc_cv_apps.cpp \
../src/apsoc_cv_camera.cpp \
../src/apsoc_cv_filter.cpp \
../src/apsoc_cv_main.cpp \
../src/apsoc_cv_vdma.cpp 

OBJS += \
./src/apsoc_cv_apps.o \
./src/apsoc_cv_camera.o \
./src/apsoc_cv_filter.o \
./src/apsoc_cv_main.o \
./src/apsoc_cv_vdma.o 

CPP_DEPS += \
./src/apsoc_cv_apps.d \
./src/apsoc_cv_camera.d \
./src/apsoc_cv_filter.d \
./src/apsoc_cv_main.d \
./src/apsoc_cv_vdma.d 


# Each subdirectory must supply rules for building sources it contributes
src/%.o: ../src/%.cpp
	@echo 'Building file: $<'
	@echo 'Invoking: ARM Linux g++ compiler'
	arm-xilinx-linux-gnueabi-g++ -Wall -O0 -g3 -I/home/mlee/Workspaces/ZedBoard/Zed_resource/opencv_includes -c -fmessage-length=0 -MT"$@" -MMD -MP -MF"$(@:%.o=%.d)" -MT"$(@:%.o=%.d)" -o "$@" "$<"
	@echo 'Finished building: $<'
	@echo ' '


