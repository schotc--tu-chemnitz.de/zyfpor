############################################################
## This file is generated automatically by Vivado HLS.
## Please DO NOT edit it.
## Copyright (C) 2014 Xilinx Inc. All rights reserved.
############################################################
open_project zyfpor_ip
set_top image_filter
add_files zyfpor_ip_top.cpp
add_files zyfpor_ip_top.h
add_files -tb zyfpor_ip_test.cpp
open_solution "solution1"
set_part {xc7z020clg484-1}
create_clock -period 10 -name default
source "./zyfpor_ip/solution1/directives.tcl"
csim_design -clean
csynth_design
cosim_design
export_design -format ip_catalog
