/*
 * ZYFPOR - Zynq Flexible Platform for Object Recognition
 * zyfpor_ip_test.cpp
 *
 *
 * Author	: Christian Schott
 * E-Mail	: christian.schott@etit.tu-chemnitz.de
 * Company	: University of Technology Chemnitz
 *
 * derived from Xilinx XAPP1167
 *
 * created on: April 22, 2015
 *
 */

#include "zyfpor_ip_top.h"
#include "hls_opencv.h"
#include <stdio.h>

#include <opencv2/core/core.hpp>
#include <opencv2/highgui/highgui.hpp>

using namespace std;


int main () {

	// if nothing is defined --> iplimage video input is used!!!
	//#define use_iplimage;

	t_lines_angle lines_angle_tb[MAX_LINES];
	t_lines_rho lines_rho_tb[MAX_LINES];

	hls::Polar_< t_lines_angle, t_lines_rho> lines_tb[MAX_LINES];
	unsigned int lines_size_tb;

	float theta;
	float rho;

	#if defined use_iplimage
	{
		// image input
		// comment out the respective test image
		//IplImage* src = cvLoadImage("testimages/test_1080p.bmp");
		//IplImage* src = cvLoadImage("testimages/Lena.bmp");
		IplImage* src = cvLoadImage("testimages/checkerboard.bmp");
		IplImage* dst = cvCreateImage(cvGetSize(src), src->depth, 1);

		cv::namedWindow("Input Image");
		cv::namedWindow("Output Image");

		// processing in Hardware
		AXI_STREAM  src_axi, dst_axi;
		IplImage2AXIvideo(src, src_axi);
		image_filter(src_axi, dst_axi, src->height, src->width);
		AXIvideo2IplImage(dst_axi, dst);

		//Showing the images and conversion to cv::Mat
		cvShowImage("Input Image", src);
		cvShowImage("Output Image", dst);

		cv::waitKey(0);			// necessary to show the images using imshow!!!

		cvReleaseImage(&src);
		cvReleaseImage(&dst);

		return 0;
	}
	#else
	{
		// comment out the respective test sequence
		//CvCapture* cap = cvCaptureFromFile("testimages/yellow.avi");
		//CvCapture* cap = cvCaptureFromFile("testimages/video.mov");
		CvCapture* cap = cvCaptureFromFile("testimages/sequences_a5100/00001.mts");
		//CvCapture* cap = cvCaptureFromFile("testimages/sequences_C910/video_1.mov");
		cv::Mat grayImage;
		IplImage* src;
		IplImage* test = NULL;
		IplImage* dst = NULL;

		unsigned int number_pixels_on_line = 250;

		AXI_STREAM_IN  src_axi;
		AXI_STREAM_OUT dst_axi;

		cvNamedWindow("Input Video");
		cvNamedWindow("Output Video");

		if (!cap) {
			cout << "Can not open a capture object." << endl;
			return -1;
		}

		for(;;) {

			src = cvQueryFrame(cap);

			if (!src) {
				cout << "Can not query frames." << endl;
				return -1;
			}

			// processing in Hardware
			IplImage2AXIvideo(src, src_axi);
			image_filter(src_axi, dst_axi, src->height, src->width, number_pixels_on_line, lines_angle_tb, lines_rho_tb);

			if(dst == NULL)
				dst = cvCreateImage(cvSize(src->width, src->height), IPL_DEPTH_8U, 1);	//src->nChannels
			if(test == NULL)
				test = cvCreateImage(cvSize(src->width, src->height), IPL_DEPTH_8U, 1);	//src->nChannels
			AXIvideo2IplImage(dst_axi, dst);

			cvCopy(dst, test);

			// read out lines angle and rho
			for( unsigned int i = 0; i < MAX_LINES; i++ )
			{
			   	theta = lines_angle_tb[i].to_float();
			    rho = lines_rho_tb[i].to_float();

			    cv::Point pt1, pt2;
			    double a = cos(theta), b = sin(theta);
			    double x0 = a*rho, y0 = b*rho;
			    pt1.x = cvRound(x0 + 1000*(-b));
			    pt1.y = cvRound(y0 + 1000*(a));
			    pt2.x = cvRound(x0 - 1000*(-b));
			    pt2.y = cvRound(y0 - 1000*(a));
			    cvLine(dst, pt1, pt2, cv::Scalar(255,0,0), 3, 8, 0);
			}

			cvShowImage("Input Video", src);
			cvShowImage("Output Video", dst);

			if (cv::waitKey(30) >= 0)
				break;
		}

		cvReleaseCapture(&cap);
		cvReleaseImage(&src);
		cvReleaseImage(&dst);
		cvDestroyAllWindows();

	}
	#endif

	return 0;
}
