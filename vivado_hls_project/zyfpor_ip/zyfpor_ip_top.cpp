/*
 * ZYFPOR - Zynq Flexible Platform for Object Recognition
 * zyfpor_ip_top.cpp
 *
 *
 * Author	: Christian Schott
 * E-Mail	: christian.schott@etit.tu-chemnitz.de
 * Company	: University of Technology Chemnitz
 *
 * derived from Xilinx XAPP1167
 *
 * created on: April 22, 2015
 *
 */

#include "zyfpor_ip_top.h"

void image_filter(AXI_STREAM_IN& INPUT_STREAM, AXI_STREAM_OUT& OUTPUT_STREAM, int rows, int cols, unsigned int number_pixels_line, t_lines_angle lines_angle[MAX_LINES], t_lines_rho lines_rho[MAX_LINES]){

//Create AXI streaming interfaces for the core
#pragma HLS INTERFACE axis port=INPUT_STREAM
#pragma HLS INTERFACE axis port=OUTPUT_STREAM

// pragma RESOURCE
#pragma HLS RESOURCE core=AXI_SLAVE variable=rows metadata="-bus_bundle CONTROL_BUS"
#pragma HLS RESOURCE core=AXI_SLAVE variable=cols metadata="-bus_bundle CONTROL_BUS"
#pragma HLS RESOURCE core=AXI_SLAVE variable=number_pixels_line metadata="-bus_bundle CONTROL_BUS"
#pragma HLS RESOURCE core=AXI_SLAVE variable=return metadata="-bus_bundle CONTROL_BUS"
#pragma HLS RESOURCE variable=lines_angle core=RAM_1P_BRAM metadata="-bus_bundle CONTROL_BUS"
#pragma HLS RESOURCE variable=lines_rho core=RAM_1P_BRAM metadata="-bus_bundle CONTROL_BUS"

// pragma INTERFACE
#pragma HLS INTERFACE ap_stable port=rows
#pragma HLS INTERFACE ap_stable port=cols
#pragma HLS INTERFACE ap_stable port=number_pixels_line
#pragma HLS INTERFACE ap_memory port=lines_angle
#pragma HLS INTERFACE ap_memory port=lines_rho


    RGB_IMAGE input_img(rows, cols);

    GRAY_IMAGE gray_img(rows, cols);
    GRAY_IMAGE gray_img_reduced_noise(rows, cols);
    GRAY_IMAGE gray_img1(rows, cols);
    GRAY_IMAGE gray_img2(rows, cols);
    GRAY_IMAGE sobel_out_hori(rows, cols);
    GRAY_IMAGE sobel_out_vert(rows, cols);
    GRAY_IMAGE sobel_out(rows, cols);
    GRAY_IMAGE hough_in(rows, cols);
    GRAY_IMAGE threshold_image(rows, cols);
    GRAY_IMAGE eroded_image(rows, cols);
    GRAY_IMAGE output_img_gray(rows, cols);

    hls::Polar_< t_lines_angle, t_lines_rho> lines_internal[MAX_LINES];


#pragma HLS dataflow
    // input stream has a width of 24 Bit (RGB)
    hls::AXIvideo2Mat(INPUT_STREAM, input_img);
    hls::CvtColor<HLS_BGR2GRAY>(input_img, gray_img);
    hls::GaussianBlur<5,5>(gray_img, gray_img_reduced_noise);		// does not work with kernel size of 3x3
    hls::Duplicate(gray_img_reduced_noise, gray_img1, gray_img2);
    hls::Sobel<1,0,3>(gray_img1, sobel_out_hori);
    hls::Sobel<0,1,3>(gray_img2, sobel_out_vert);

    // combine the 2 sobel images to one using AddWeighted
    hls::AddWeighted(sobel_out_hori, 1, sobel_out_vert, 1, 0, sobel_out);

    hls::Threshold(sobel_out, threshold_image, 20, 255, HLS_THRESH_BINARY);

    hls::Erode(threshold_image, eroded_image);

    hls::Duplicate(eroded_image, hough_in, output_img_gray);

    hls::HoughLines2<3,5>(hough_in, lines_internal, number_pixels_line);

    // write data of lines to output
    for (int i = 0; i < MAX_LINES; i++)
    {
    	lines_angle[i] = lines_internal[i].angle;
    	lines_rho[i] = lines_internal[i].rho;
    }

    // output stream has a width of 8 Bit (Grayscale)
    hls::Mat2AXIvideo(output_img_gray, OUTPUT_STREAM);
}
